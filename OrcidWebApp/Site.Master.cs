﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace OrcidWebApp
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        // Unique key to use as parameter for Javascript and CSS to prevent browser cache
        public static string UniqueKey;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                UniqueKey = GetMd5Hash(md5Hash, DateTime.Now.ToLongTimeString());
            }
        }

        public string ResolveURL(string url)
        {
            var resolvedURL = this.Page.ResolveClientUrl(url);
            return resolvedURL;
        }

        // Provide a link element to local CSS file with unique key to prevent cache
        public string cssLink(string cssURL)
        {
            return string.Format("<link href='{0}?r={1}' rel='stylesheet' type='text/css'>", ResolveURL(cssURL), UniqueKey);
        }

        /// <summary>
        /// Returns an MD5 Hash from a string
        /// </summary>
        /// <param name="md5Hash">MD5 Hash Object</param>
        /// <param name="input">String to encode</param>
        /// <returns>MD5 Hash</returns>
        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    }
}