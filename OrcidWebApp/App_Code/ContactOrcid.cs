﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

    public class ContactOrcid
    {
        public static string contactOrcidSupport(string fullName, string cid, string email, string comments)
        {
            string orcidContactEmail = ConfigurationManager.AppSettings["orcidContactEmail"];
            string orcidContactEmailCC = ConfigurationManager.AppSettings["orcidContactEmailCC"];
            string orcidContactSmtp = ConfigurationManager.AppSettings["orcidContactSmtp"];
            
            try
            {
                MailMessage mail = new MailMessage();
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.From = new MailAddress(email);
                mail.To.Add(orcidContactEmail);
                mail.CC.Add(orcidContactEmailCC);
                mail.Subject = "[Chalmers ORCID support] New support request";
                mail.IsBodyHtml = true;
                mail.Body = "New request from Chalmers ORCID support contact form: <br /><br />";
                mail.Body += "Name: " + fullName + "<br />";
                mail.Body += "CID: " + cid + "<br />";
                mail.Body += "Email: " + email + "<br /><br />";
                mail.Body += "Comments / question: " + comments + "<br />";
                SmtpClient smtp = new SmtpClient();
                smtp.Host = orcidContactSmtp;
                //smtp.Port = 25;
                smtp.Send(mail);

                return "success";
            }
            catch(Exception e)
            {
                return "error: " + e.Message;  
            }

        }
    }
