﻿using System;
using System.Web.UI;
using System.Configuration;
using System.Security.Claims;
using System.Text.RegularExpressions;

namespace OrcidWebApp
{
    public class Utils
    { 
        /// <summary>
        /// Check if the current user on page is authenticated
        /// </summary>
        /// <param name="page">Current Page</param>
        /// <returns>True or false</returns>
        public static bool IsAuthenticated(Page page)
        {
            ClaimsPrincipal princ = page.User as ClaimsPrincipal;
            ClaimsIdentity ident = princ.Identity as ClaimsIdentity;

            return ident.IsAuthenticated;
        }

        /// <summary>
        /// Fetch the authenticated Chalmers ID (cid) from the identity provider, if any.
        /// </summary>
        /// <param name="page">Current Page</param>
        /// <returns>cid or null</returns>
        public static string getAuthCid(Page page)
        {
            ClaimsPrincipal princ = page.User as ClaimsPrincipal;
            ClaimsIdentity ident = princ.Identity as ClaimsIdentity;

            if (ident.IsAuthenticated)
            {
                string authUser = ident.FindFirst(ClaimTypes.Upn).Value;
                string authCid = authUser.Split('@')[0];
                return authCid;
            }
            return null;
        }

        /// <summary>
        /// Fetch the authenticated user's name from the identity provider, if any.
        /// </summary>
        /// <param name="page">Current Page</param>
        /// <returns>Name of the user or null</returns>
        public static string GetAuthName(Page page)
        {
            ClaimsPrincipal princ = page.User as ClaimsPrincipal;
            ClaimsIdentity ident = princ.Identity as ClaimsIdentity;

            if (ident.IsAuthenticated)
            {
                return ident.FindFirst(ClaimTypes.Name).Value;
            }
            return null;
        }

        /// <summary>
        /// Return a URI to use towards ORCID in order to register an existing Orcid id.
        /// </summary>
        /// <returns>uri</returns>
        public static string getOrcidAuthUri()
        {
            string orcidUri = ConfigurationManager.AppSettings["orcidAuthEp"];
            string appOrcid = ConfigurationManager.AppSettings["appOrcid"];
            string responseType = "code";
            string orcidScope = "/read-limited";
            string redirUri = ConfigurationManager.AppSettings["redirUri"];

            return orcidUri + "?client_id=" + appOrcid + "&response_type=" + responseType + "&scope=" + orcidScope + "&redirect_uri=" + redirUri;
        }

        /// <summary>
        /// Validates the format of an ORCID
        /// </summary>
        /// <param name="page">orcid ID</param>
        /// <returns>true or false</returns>
        public static bool IsValidOrcid(string orcid)
        {
            Regex rgx = new Regex(@"[0-9\-Xx]");
            orcid = orcid.Trim();
            string orcidLastDigit = orcid.Substring(orcid.Length - 1, 1).ToUpper();
            string orcidCheckSum = GetOrcidCheckSum((orcid.Replace("-", "")).Remove(orcid.Replace("-", "").Length - 1));

            if (orcid.Length == 19 && rgx.IsMatch(orcid) && orcidCheckSum == orcidLastDigit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get the checksum of an ORCID
        /// </summary>
        /// <param name="page">orcid ID base digits (15), without hyphens</param>
        /// <returns>ORCID checksum</returns>
        public static string GetOrcidCheckSum(string baseDigits)
        {
            int total = 0;
            char[] baseDigitArray = baseDigits.ToCharArray();

            for (int i = 0; i < baseDigitArray.Length; i++)
            {
                int digit = Convert.ToInt32(char.GetNumericValue(baseDigitArray[i]));
                total = (total + digit) * 2;
            }
            int remainder = total % 11;
            int result = (12 - remainder) % 11;
            return result == 10 ? "X" : result.ToString();
        }

    }
}