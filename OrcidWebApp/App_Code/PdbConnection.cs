﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrcidWebApp.PDB;

namespace OrcidWebApp
{
    /// <summary>
    /// Class for holding information about persons
    /// </summary>
    public class PersonInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Cid { get; set; }
        public string Orcid { get; set; }
        public string Email { get; set; }
    }

    /// <summary>
    /// Summary description for PdbConnection
    /// </summary>
    public class PdbConnection
    {
        private string sessionId = string.Empty;
        private pdbV3R3MscompatPortTypeClient client = null;

        public PdbConnection(string url, string username, string password)
        {
            client = new pdbV3R3MscompatPortTypeClient("pdbV3R3MscompatservicePort", url);
            sessionId = client.sessionStart(new sessionStartRequest()).value;

            client.sessionAuthLogin(new sessionAuthLoginRequest(sessionId, username, password));
        }

        public void Close()
        {
            client.sessionStop(new sessionStopRequest(sessionId));
            client = null;
        }

        public PersonInfo getInfoByAccount(string accountName)
        {
            var search = new newPersonSearchOptionsType();
            search.optionalElements = new newPersonSearchOptionsTypeOptionalElements();
            search.optionalElements.accountSearch = new newAccountSearchOptionsType();
            search.optionalElements.accountSearch.optionalElements = new newAccountSearchOptionsTypeOptionalElements();
            search.optionalElements.accountSearch.optionalElements.account = accountName;

            return getInfoFromSearch(search);
        }

        public PersonInfo getInfoByOrcid(string orcid)
        {
            var search = new newPersonSearchOptionsType()
            {
                optionalElements = new newPersonSearchOptionsTypeOptionalElements()
                {
                    orcidPattern = "*" + orcid
                }
            };

            return getInfoFromSearch(search);
        }

        private PersonInfo getInfoFromSearch(newPersonSearchOptionsType search)
        {
            var tmpl = new personDataTemplateType()
            {
                optionalElements = new personDataTemplateTypeOptionalElements()
                {
                    firstname = true,
                    firstnameSpecified = true,
                    lastname = true,
                    lastnameSpecified = true,
                    email = true,
                    emailSpecified = true,
                    orcid = true,
                    orcidSpecified = true,
                    primaryAccount = true,
                    primaryAccountSpecified = true
                }
            };

            var resp = client.personDig(new personDigRequest(sessionId, search, tmpl));
            if (resp.personTemplatedData == null)
                throw new IndexOutOfRangeException();

            var data = resp.personTemplatedData[0].optionalElements;

            PersonInfo ret = new PersonInfo()
            {
                FirstName = data.firstname,
                LastName = data.lastname,
                Cid = data.primaryAccount
            };

            if (data.email != null)
                ret.Email = (string)data.email.Item;

            if (data.orcid != null)
                ret.Orcid = (string)data.orcid.Item;

            return ret;
        }


        public void orcidSave(string orcid, string cid)
        {
            client.personSetOrcid(new personSetOrcidRequest() { orcid_in = orcid, person_in = cid, session_in = sessionId });

        }
    }
}
