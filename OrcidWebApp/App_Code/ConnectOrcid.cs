﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

using OrcidWebApp;

/// <summary>
/// Stores an ORCID in Chalmers PDB
/// </summary>
public class ConnectOrcid
{
    /// <summary>
    ///  
    /// </summary>
    /// <param name="auth_code">Authentication code for orcid api</param>
    /// <returns>A string on the format orcid@firstName@lastName</returns>
    public static string getOrcidProfile(string auth_code)
    {
        // string orcidProfileData = string.Empty;

        string clientID = ConfigurationManager.AppSettings["clientID"];
        string clientSecret = ConfigurationManager.AppSettings["clientSecret"];
        string redirUri = ConfigurationManager.AppSettings["redirUri"];
        string grantType = "authorization_code";
        string pdbEndpoint = ConfigurationManager.AppSettings["pdbEP"];
        string pdbUser = ConfigurationManager.AppSettings["pdbUser"];
        string pdbPw = ConfigurationManager.AppSettings["pdbPw"];
        string orcidOauthURL = ConfigurationManager.AppSettings["orcidOauthEp"];
        string orcidAPIBaseURI = ConfigurationManager.AppSettings["orcidAPIBaseURI"];
        //string orcidAPIBaseURI = ConfigurationManager.AppSettings["orcidPubAPIBaseURI"];

        string postdata = "client_id=" + clientID + "&client_secret=" + clientSecret + "&grant_type=" + grantType +
                          "&code=" + auth_code + "&redirect_uri=" + redirUri;
        byte[] buffer = Encoding.UTF8.GetBytes(postdata);

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(orcidOauthURL);
        request.Method = "POST";
        request.ContentLength = buffer.Length;
        request.ContentType = "application/x-www-form-urlencoded";
        request.Accept = "application/json";

        Stream postData = request.GetRequestStream();
        postData.Write(buffer, 0, buffer.Length);
        postData.Close();

        // if we get a response 
        try
        {
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            // Read the JSON content.
            var jsonString = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();

            JavaScriptSerializer js = new JavaScriptSerializer();
            dynamic json = js.Deserialize<dynamic>(jsonString);

            string orcid = json["orcid"];
            string access_token = json["access_token"];
            string refresh_token = json["refresh_token"];

            //string orcidProfileURL = orcidAPIBaseURI + orcid + "/orcid-profile";
            string orcidRecordPersonURL = orcidAPIBaseURI + orcid + "/person";
            var firstName = string.Empty;
            var lastName = string.Empty;

            try
            {
                HttpWebRequest pRequest = (HttpWebRequest)WebRequest.Create(orcidRecordPersonURL);
                pRequest.Method = "GET";
                pRequest.ContentType = "application/vdn.orcid+xml";
                pRequest.Headers.Add("Authorization:Bearer " + access_token);
                pRequest.Accept = "application/json";

                // if we get a response
                try
                {
                    WebResponse pResponse = pRequest.GetResponse();
                    Stream dataStreamRecord = pResponse.GetResponseStream();
                    StreamReader readerRecord = new StreamReader(dataStreamRecord);

                    // Read the JSON content.
                    var jsonStringRecord = readerRecord.ReadToEnd();
                    readerRecord.Close();
                    dataStreamRecord.Close();
                    pResponse.Close();

                    JavaScriptSerializer jsRecord = new JavaScriptSerializer();
                    dynamic jsonRecord = jsRecord.Deserialize<dynamic>(jsonStringRecord);

                    if (jsonRecord["name"] != null)
                    {
                        //var person = jsonRecord.name;
                        firstName = jsonRecord["name"]["given-names"]["value"];
                        lastName = jsonRecord["name"]["family-name"]["value"];
                    }
                    
                    string orcidProfileData = orcid + "@" + firstName + "@" + lastName;
                    return orcidProfileData;
                }
                catch (WebException e)
                {
                    return "Error - no ORCID profile found: " + e.Message + "orcid: " + orcid;
                }
                
            }
            catch (WebException e)
            {
                return "error - no ORCID profile found: " + e.Message + "orcid: " + orcid;
            }
            //catch (Exception e)
            //{
            //    return "error: " + e.Message;
            //}        
        }            
        catch (Exception e)
        {
            return "error getting ORCID from auth_code: " + e.Message;
        }            
    }

    /// <summary>
    /// Sets given orcid for given accountname in PDB
    /// CID should be available from IdP. If not, check if there is a temporary ("?") Orcid in PDB (not available from v 2.x -) 
    /// </summary>
    /// <param name="orcid">Orcid to set</param>
    /// <param name="cid">Cid to set orcid on</param>
    /// <returns>String "success" or an error message</returns>
    public static string connectOrcidtoPDB(string orcid, string cid)
    {
        string pdbEndpoint = ConfigurationManager.AppSettings["pdbEP"];
        string pdbUser = ConfigurationManager.AppSettings["pdbUser"];
        string pdbPw = ConfigurationManager.AppSettings["pdbPw"];
 
            // save new or update ORCID in PDB
            try
            {
                var pdbConn = new PdbConnection(pdbEndpoint, pdbUser, pdbPw);

                if (string.IsNullOrEmpty(cid))
                {
                    try
                    {
                        string tmpOrcid = "?" + orcid;
                        var accountOrcid = pdbConn.getInfoByOrcid(tmpOrcid);

                        // No (tmp) Orcid found in PDB, we cannot proceed.
                        if (accountOrcid == null)
                        {
                            return "Error: No CID supplied and no ORCID found in PDB.";
                        }
                        else
                        {
                            cid = accountOrcid.Cid;
                        }
                    }
                    catch (Exception e)
                    {
                        return "Error: No CID supplied and no ORCID found in PDB." + e.Message;
                    }
                }

                pdbConn.orcidSave(orcid, cid);
                return "success";
            }
            catch (Exception ex)
            {
                return "Error saving ORCID in PDB - is ORCID already in PDB?" + ex.Message;
            }
    }
}