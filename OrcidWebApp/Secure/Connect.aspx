﻿<%@ Page Title="Connect your ORCID with Chalmers" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Connect.aspx.cs" Inherits="OrcidWebApp.Secure.Connect" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="jumbotron">
        <h1>Connect your ORCID</h1>
        <asp:Panel ID="OrcidLink" runat="server">
            <p class="lead">
                Hello <%= AuthName %> (<%= AuthCid %>).<br />
                <br />
                <a href="<%= OrcidAuthUri %>">Please click here and follow the instructions</a>
            </p>
            <p>
                Not you? Please restart your browser and try again.
            </p>
        </asp:Panel>
        <asp:Panel ID="OrcidLinkError" runat="server">
            <p>
                Error: CID is missing
            </p>
        </asp:Panel>
    </div>
</asp:Content>
