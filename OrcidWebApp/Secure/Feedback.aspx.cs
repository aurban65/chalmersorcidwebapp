﻿using System;
using System.Configuration;
using System.Web;

namespace OrcidWebApp.Secure
{
    public partial class Feedback : System.Web.UI.Page
    {

        public string authUser = null;
        public string orcidNewLink = "";
        public string orcidBaseURI = ConfigurationManager.AppSettings["orcidBaseURI"];
        public string authCode = String.IsNullOrEmpty(HttpContext.Current.Request["code"]) ? "" : HttpContext.Current.Request["code"];
        public string errorCode = String.IsNullOrEmpty(HttpContext.Current.Request["error"]) ? "" : HttpContext.Current.Request["error"];
        public string IsFinalStep = String.IsNullOrEmpty(HttpContext.Current.Request["finalStep"]) ? "" : HttpContext.Current.Request["finalStep"];

       protected void Page_Load(object sender, EventArgs e)
    {
        var cid = Utils.getAuthCid(Page); // CID from IDP 

        OrcidSuccess.Visible = false;
        OrcidDenied.Visible = false;
        OrcidDisaster.Visible = false;
        OrcidIdentity.Visible = false;

        if (authCode.Length > 0 && authCode != null) // we got an auth_code
        {
            // make sure that this is the correct user (CID) before attempting connect
            // fetch ORCID profile data as well
            string orcidProfileData = ConnectOrcid.getOrcidProfile(authCode);
            
            //orcidID.Text = orcidProfileData;
            //OrcidIdentity.Visible = true;

            if (orcidProfileData.Length > 4 && orcidProfileData != null && orcidProfileData.IndexOf("@") > 0)
            {
                string orcid = orcidProfileData.Split('@')[0];
                string firstName = orcidProfileData.Split('@')[1];
                string lastName = orcidProfileData.Split('@')[2];

                cidHid.Value = cid;
                orcidHid.Value = orcid;
                orcidID.Text = orcid;
                orcidName.Text = firstName + " " + lastName;
                OrcidIdentity.Visible = true;
            }
            else  // Something was wrong, try again
            {
                string orcidAuthUriTry = Utils.getOrcidAuthUri();
                connectError.Text = orcidProfileData;
                orcidLinkUriTry.Text = "<a href=\"" + orcidAuthUriTry + "\">Click here to connect your ORCID with Chalmers</a>";
                OrcidDisaster.Visible = true;
            }
        }
            else if (errorCode == "access_denied") // The user denied Chalmers access to his/her orcid
        {
            string orcidAuthUri = Utils.getOrcidAuthUri();
            orcidLinkUri.Text = "<a href=\"" + orcidAuthUri + "\">Click here to connect your ORCID with Chalmers</a>";
            OrcidDenied.Visible = true;
        }
        else if (IsFinalStep == "finalTrue")
        {
            OrcidDisaster.Visible = false;
        }
        else
        {
            OrcidDisaster.Visible = true; // something else went wrong
        }
    }

        protected void connectOrcidForm_Click(object sender, EventArgs e)
         {

            //string authCodeF = authCode;
            //string cidF = HttpContext.Current.Request["cidHid"];
            string orcidF = orcidHid.Value;
            string cidF = cidHid.Value;

            string OrcidConnect = ConnectOrcid.connectOrcidtoPDB(orcidF, cidF);

            // an ORCID is returned
            if (OrcidConnect == "success")
            {
                orcidNewLink = orcidBaseURI + orcidF;
                OrcidIdentity.Visible = false;
                OrcidSuccess.Visible = true;
            }
            else
            {
                OrcidDisaster.Visible = true; // something failed in Orcid/PDB connection
                connectError.Text = "ConnectOrcid returned: " + OrcidConnect; 
            }
         }
    }
}