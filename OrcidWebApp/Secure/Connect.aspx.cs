﻿using System;

namespace OrcidWebApp.Secure
{
    public partial class Connect : System.Web.UI.Page
    {
        public string AuthCid;
        public string AuthName;
        public string OrcidAuthUri;

        protected void Page_Load(object sender, EventArgs e)
        {
            OrcidLink.Visible = false;
            OrcidLinkError.Visible = false;

            // Fetch CID and Name for the Jumbotron
            AuthCid = Utils.getAuthCid(Page);
            AuthName = Utils.GetAuthName(Page);

            // Check if we have a CID, otherwise display error
            try
            {
                if (!string.IsNullOrEmpty(AuthCid))
                {
                    OrcidAuthUri = Utils.getOrcidAuthUri();
                    OrcidLink.Visible = true;
                }
                else
                {
                    OrcidLinkError.Visible = true;
                }
            }
            catch
            {
                OrcidLinkError.Visible = true;
            }
        }
    }
}