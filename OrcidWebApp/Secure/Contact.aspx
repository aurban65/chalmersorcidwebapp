﻿<%@ Page Title="Contact Chalmers ORCID Support" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="OrcidWebApp.Secure.Contact" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
        <div class="jumbotron">
            <h1>Contact Chalmers ORCID support</h1>
            <p class="lead"><asp:Literal ID="printReturnsC" runat="server" /></p>
            <div>
                <asp:Panel ID="newContactForm" runat="server">
                    <form id="Form1" runat="server" class="form-horizontal" role="form">
				        <div class="row">
					        <div class="col-lg-11 col-lg-offset-1 formjumbo" id="verify">
                                <asp:HiddenField ID="firstNameC" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="lastNameC" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="cidC" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="emailC" runat="server"></asp:HiddenField>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Name: </label>
                                    <div class="col-lg-6 static-form-field"><asp:Literal ID="pdbNameC" runat="server"></asp:Literal></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Email: </label>
                                    <div class="col-lg-6 static-form-field"><asp:Literal ID="pdbEmailC" runat="server"></asp:Literal></div>
                                </div>
			        	        <div class="form-group">
						            <label for="commentsC" class="col-lg-5 control-label">Comments / question:</label>
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="commentsC" runat="server" TextMode="multiline" Rows="3" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
							        <div class="col-lg-offset-2 col-lg-6">
							        </div>
						        </div>
                            </div>
                        </div>
                        <p>
							<asp:Button ID="ButtonC" runat="server" Text="Contact us" OnClick="contactSubmitForm_Click" CssClass="btn btn-lg btn-success" /> 
                        </p>
                    </form> 
                </asp:Panel>

                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 centre-text">
                       <asp:Panel ID="contactSuccess" runat="server">
                            <div>
                                <p class="lead">
                                    Thank you! Your question has been received. You will be contacted shortly. A response will be sent to <%= pdbEmailC.Text %>.<br /> 
                                </p>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="contactError" runat="server">
                            <div>
                                <p class="lead">
                                    There was an error. Please contact Chalmers ORCID Support by mail: <a href="mailto:orcid.lib@chalmers.se">orcid.lib@chalmers.se</a>
                                </p>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="contactErrorNoUser" runat="server">
                            <div>
                                <p class="lead">
                                    There was an error. User not found.
                                </p>
                            </div>
                        </asp:Panel>

                    </div>
                </div>
            </div>
        </div>
        
        
       
        



                 
            
</asp:Content>
