﻿<%@ Page Title="Chalmers Orcid Debug Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DebugPage.aspx.cs" Inherits="OrcidWebApp.Secure.DebugPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <div class="jumbotron">
        <h1>This is just a debug page</h1>
    </div>
    
    <pre>
        You are <strong><%= AuthName %></strong> with CID <strong><%= AuthCid %></strong>.
    </pre>

</asp:Content>
