﻿<%@ Page Title="Connect your ORCID with Chalmers" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Feedback.aspx.cs" Inherits="OrcidWebApp.Secure.Feedback" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <section class="featured">
        <div class="jumbotron">
            <h1>Connect your ORCID</h1>
            <asp:Panel ID="OrcidIdentity" runat="server">
                <form id="Form1" action="Feedback.aspx?finalStep=finalTrue" runat="server">
                    <p class="lead">
                        Hello <%= User.Identity.Name %>!<br />
                        Click below to verify your Chalmers connection.<br />
                        Your ORCID is 
                    <asp:Literal ID="orcidID" runat="server" />
                        and your ORCID profile name is 
                    <asp:Literal ID="orcidName" runat="server" />.<br />
                        <asp:HiddenField ID="cidHid" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="orcidHid" runat="server"></asp:HiddenField>
                    </p>
                    <p class="lead">
                        <asp:Button ID="Button2" runat="server" Text="Connect" OnClick="connectOrcidForm_Click" CssClass="btn btn-lg btn-success" />
                    </p>
                    <p>
                        Not you? Please restart your browser and try again.
                    </p>
                </form>
            </asp:Panel>

            <asp:Panel ID="OrcidSuccess" runat="server">
                <p class="lead">
                    Thank you <%= User.Identity.Name %>! Your ORCID is now connected to Chalmers.<br />
                    Please note that it may take some time for this to appear in other systems, such as CPL. 
                    <br />
                    <a href="<%= orcidNewLink %>" title="ORCID profile for <%= User.Identity.Name %>">
                        <img src="../Images/orcid_24x24.png" /><%= orcidNewLink %>
                    </a>
            </asp:Panel>

            <asp:Panel ID="OrcidDenied" runat="server">
                <p class="lead">
                    Hello <%= User.Identity.Name %>!<br />
                    You denied Chalmers read access to your ORCID profile.<br /> 
                    If this was unintended, you can click the link below and try again<br />
                    <asp:Literal ID="orcidLinkUri" runat="server"></asp:Literal>
                    <br /><br />
                    Please contact Chalmers ORCID Support if you need further information or assistance.
                </p>
            </asp:Panel>

            <asp:Panel ID="OrcidDisaster" runat="server">
                An error occured. Please try again by following this link:
                <br />
               <asp:Literal ID="orcidLinkUriTry" runat="server"></asp:Literal><br />
                Contact the library if the problem persists.<br />
                <asp:Literal ID="connectError" runat="server"></asp:Literal>
            </asp:Panel>
        </div>
    </section>
</asp:Content>
