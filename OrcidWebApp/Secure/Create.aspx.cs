﻿using System;
using System.Configuration;
using System.Web;

namespace OrcidWebApp.Secure
{
    public partial class Default : System.Web.UI.Page
    {
        string pdbEndpoint = ConfigurationManager.AppSettings["pdbEP"];
        string pdbUser = ConfigurationManager.AppSettings["pdbUser"];
        string pdbPw = ConfigurationManager.AppSettings["pdbPw"];

        public string OrcidAuthUri;

        protected void Page_Load(object sender, EventArgs e)
        {
            newCreateForm.Visible = false;
            createResultConnect.Visible = false;
            createResultInPDB.Visible = false;
            createResultSuccess.Visible = false;
            createResultError.Visible = false;
            createResultError.Visible = false;

            fetchPDBData();
        }

        protected void fetchPDBData()
        {
            try
            {
                var pdb_conn = new PdbConnection(pdbEndpoint, pdbUser, pdbPw);
                var cidIn = Utils.getAuthCid(Page); // CID from IDP 
                var count_orcid = pdb_conn.getInfoByAccount(cidIn);
                pdbCid.Text = cidIn;

                if (count_orcid.Orcid == null)
                {
                    // load new form
                    printReturns.Text = "Verify that the information below is correct.";
                    newCreateForm.Visible = true;
                    firstName.Value = count_orcid.FirstName;
                    lastName.Value = count_orcid.LastName;
                    cid.Value = count_orcid.Cid;
                    email.Value = count_orcid.Email;
                    pdbName.Text = count_orcid.FirstName + " " + count_orcid.LastName;
                    pdbEmail.Text = count_orcid.Email;
                }
                else
                {
                    //printReturns.Text = "This ORCID is already registered in Chalmers PDB: " + count_orcid.Orcid;
                    OrcidAuthUri = Utils.getOrcidAuthUri();
                    createResultInPDB.Visible = true;
                }
            }
            catch // no CID supplied
            {
                createResultError.Visible = true;
                //printReturns.Text = "Error: no CID was supplied";
            }
        }

        protected void createSubmit_Click(object sender, EventArgs e)
        {
            fetchPDBData();
        }

         protected void createSubmitForm_Click(object sender, EventArgs e)
        {
            try
            {
                var pdbConn = new PdbConnection(pdbEndpoint, pdbUser, pdbPw);

                // Constants, read from Web.config
                string orcidCreateURL = ConfigurationManager.AppSettings["orcidCreateURL"];
                string appOrcid = ConfigurationManager.AppSettings["appOrcid"];
                string authStr = ConfigurationManager.AppSettings["authStr"];
                string cidName = ConfigurationManager.AppSettings["cidName"];
                string clientID = ConfigurationManager.AppSettings["clientID"];
                string redirectURI = ConfigurationManager.AppSettings["redirUri"];

                string lnameEncoded = HttpUtility.UrlEncode(lastName.Value);
                string fnameEncoded = HttpUtility.UrlEncode(firstName.Value);
                string emailEncoded = HttpUtility.UrlEncode(pdbEmail.Text);
                string redirectURIEncoded = HttpUtility.UrlEncode(redirectURI);
                string orcidCreateURLWithParameters = String.Empty;

                // Submit form data to Orcid.
                orcidCreateURLWithParameters = orcidCreateURL + "?client_id=" + clientID + "&response_type=code&scope=/read-limited&redirect_uri=" + redirectURIEncoded + "&family_names=" + lnameEncoded + "&given_names=" + fnameEncoded + "&email=" + emailEncoded + "&lang=en&show_login=false";
                Response.Buffer = true;

                try
                {
                    Response.Redirect(orcidCreateURLWithParameters);
                }
                catch (Exception exc)
                {
                    createResultError.Visible = true;
                    newCreateForm.Visible = false;
                }
            }
            catch (Exception ex)
            {
                createResultError.Visible = true;
                newCreateForm.Visible = false;
                //printResult.Text = "An unknown error occured.";
            }
        }

    }
}
