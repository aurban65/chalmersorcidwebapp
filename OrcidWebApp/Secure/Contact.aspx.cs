﻿using System;
using System.Configuration;

namespace OrcidWebApp.Secure
{
    public partial class Contact : System.Web.UI.Page
    {
        string pdbEndpointC = ConfigurationManager.AppSettings["pdbEP"];
        string pdbUserC = ConfigurationManager.AppSettings["pdbUser"];
        string pdbPwC = ConfigurationManager.AppSettings["pdbPw"];

        protected void Page_Load(object sender, EventArgs e)
        {
            newContactForm.Visible = false;
            contactSuccess.Visible = false;
            contactError.Visible = false;
            contactErrorNoUser.Visible = false;

            fetchPDBDataConnect();
        }

        protected void fetchPDBDataConnect()
        {
            try
            {
                var pdb_conn = new PdbConnection(pdbEndpointC, pdbUserC, pdbPwC);
                var cidIn = Utils.getAuthCid(Page); // CID from IDP 
                var count_orcid = pdb_conn.getInfoByAccount(cidIn);
                
                if (count_orcid.Cid != null)
                {
                    // load new form
                    printReturnsC.Text = "Please enter your question below.";
                    newContactForm.Visible = true;
                    firstNameC.Value = count_orcid.FirstName;
                    lastNameC.Value = count_orcid.LastName;
                    cidC.Value = count_orcid.Cid;
                    emailC.Value = count_orcid.Email;
                    pdbNameC.Text = firstNameC.Value + " " + lastNameC.Value;
                    pdbEmailC.Text = count_orcid.Email;
                }
                else
                {
                    contactError.Visible = true;
                }
            }
            catch // no CID supplied
            {
                contactError.Visible = true;
                //printReturns.Text = "Error: no CID was supplied";
            }
        }

        protected void contactSubmitForm_Click(object sender, EventArgs e)
        {
            try
            {
                var pdbConn = new PdbConnection(pdbEndpointC, pdbUserC, pdbPwC);
                var contactResponse = ContactOrcid.contactOrcidSupport(pdbNameC.Text, cidC.Value, pdbEmailC.Text, commentsC.Text);
                // check if returned value is an actual ORCID, and store to PDB if so
                if (contactResponse == "success")
                {
                    contactSuccess.Visible = true;
                    newContactForm.Visible = false;
                    printReturnsC.Visible = false;
                }
                else // 
                {
                    contactError.Visible = true;
                    newContactForm.Visible = true;
                }
            }
            catch (Exception ex)
            {
                contactError.Visible = true;
                newContactForm.Visible = false;
                //printResult.Text = "An unknown error occured.";
            }

        }

    }

}