﻿<%@ Page Title="Create a new ORCID" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="OrcidWebApp.Secure.Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="jumbotron">
        <h1>Create a new ORCID</h1>
        <p class="lead">
            <asp:Literal ID="printReturns" runat="server" />
        </p>

        <asp:Panel ID="newCreateForm" runat="server">
            <form id="Form1" runat="server" class="form-horizontal" role="form">
                <div class="row">
                    <div class="col-lg-12 formjumbo" id="verify">
                        <asp:HiddenField ID="firstName" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="lastName" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="cid" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="email" runat="server"></asp:HiddenField>
                        <div class="form-group">
                            <label class="col-lg-6 control-label">Name: </label>
                            <div class="col-lg-6 static-form-field">
                                <asp:Literal ID="pdbName" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-6 control-label">CID: </label>
                            <div class="col-lg-6 static-form-field">
                                <asp:Literal ID="pdbCid" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-6 control-label">Email: </label>
                            <div class="col-lg-6 static-form-field">
                                <asp:Literal ID="pdbEmail" runat="server"></asp:Literal>
                            </div>
                        </div>
                     </div>
                </div>
                <p>
                    <asp:Button ID="Button2" runat="server" Text="Create Orcid" OnClick="createSubmitForm_Click" CssClass="btn btn-lg btn-success" />
                </p>
            </form>
        </asp:Panel>
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 centre-text">
                <asp:Panel ID="createResultSuccess" runat="server">
                    <div>
                        <p class="lead">
                            ORCID was succesfully created.
                        </p> 
                        <p class="lead">
                            A verification e-mail has been sent to <%= pdbEmail.Text %>.<br />
                            Follow the instructions to complete your registration.
                        </p>
                    </div>
                </asp:Panel>
                <asp:Panel ID="createResultInPDB" runat="server">
                    <div>
                        <p class="lead">
                            Hello <%= User.Identity.Name %> (<%= pdbCid.Text %>).<br />
                            You seem to have an ORCID already.<br />
                            Please <a href="<%= OrcidAuthUri %>">connect this to Chalmers</a>
                        </p>
                        <p>
                            Not you? Please restart your browser and try again.
                        </p>
                    </div>
                </asp:Panel>
                <asp:Panel ID="createResultConnect" runat="server">
                    <div>
                        <p class="lead">
                            ORCID could not be created.<br />
                            If you already have an ORCID, please <a href="<%= OrcidAuthUri %>">connect to Chalmers</a>.
                        </p>
                    </div>
                </asp:Panel>
                <asp:Panel ID="createResultError" runat="server">
                    <div>
                        <p class="lead">
                            ORCID could not be created.<br />
                            An unknown error occured.
                        </p>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>
