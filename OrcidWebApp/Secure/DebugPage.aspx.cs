﻿using System;
using System.Security.Claims;

namespace OrcidWebApp.Secure
{
    public partial class DebugPage : System.Web.UI.Page
    {
        public bool IsAuthenticated;
        public string AuthCid;
        public string AuthName;

        protected void Page_Load(object sender, EventArgs e)
        {
            IsAuthenticated = Utils.IsAuthenticated(Page);
            AuthCid = Utils.getAuthCid(Page);
            AuthName = Utils.GetAuthName(Page);
        }
    }
}