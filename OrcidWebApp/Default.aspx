﻿<%@ Page Title="Create and connect your ORCID with Chalmers" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OrcidWebApp.Default" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <%
        // Redirect user if we're not serving over https://orcid.chalmers.se, localhost not included 
        if ((Request.ServerVariables["SERVER_PORT_SECURE"] == "0" && Request.ServerVariables["SERVER_NAME"] != "localhost") ||
            (Request.ServerVariables["SERVER_PORT_SECURE"] == "1" && Request.ServerVariables["SERVER_NAME"] == "orcid.lib.chalmers.se"))
        {
            Response.RedirectPermanent("https://orcid.chalmers.se");
        }
    %>
    <div class="jumbotron">
        <h1>Create and connect your ORCID to Chalmers</h1>
        <p class="lead">
            This is where you as a researcher at Chalmers create a new ORCID and connect it to Chalmers. 
            If you already have an ORCID, just connect it to Chalmers.
        </p>
        <p>
            <a class="btn btn-lg btn-success" href="secure/Create.aspx">Create a new ORCID</a>
            <a class="btn btn-lg btn-success" href="secure/Connect.aspx">Connect existing ORCID</a>
		</p>
    </div>
</asp:Content>
