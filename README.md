* ChalmersOrcidWebApp

* Web app with the purpose of assisting researchers at Chalmers University of Technology in creating an ORCiD ID and/or connect this, or an existing ID to Chalmers local research, publication and HR systems.
* The code supplied contains bits and pieces that are specific to Chalmers and the local HR system (PDB), and should be regarded as an example, rather than a working, fully functional app that would work out of the box.
* The app has been rewritten to comply with the latest version of the ORCiD API (2.0). The workflow is more or less an adaption of the recommended solution, described at https://members.orcid.org/api/integrate/create-records.
* A live app is up and running at http://orcid.chalmers.se. Using it requires a Chalmers ID (CID).


* orcid.lib@chalmers.se
* More information about ORCiD at http://orcid.org
